import {Dimensions} from "react-native";

const windowHeight = Dimensions.get("window").height;
const windowWidth = Dimensions.get("window").width;

export default {
  get colorBoxHeight() {
    return windowHeight - this.buttonGroupHeight - this.playScreenHeaderHeight - 30;
  },

  get colorBoxWidth() {
    return windowWidth;
  },

  playScreenHeaderHeight: 200,

  rowBackgroundColors: ["#ff8080", "#cc0000", "#b36b00", "#ffcc00", "#196619", "#005ce6", "#990099", "#cc0052", "#2eb82e", "#00cccc", "#ff8080"],

  firstRowBackgroundColor: "#cc0000",
  secondRowBackgroundColor: "#b36b00",
  thirdRowBackgroundColor: "#ffcc00",
  fourthRowBackgroundColor: "#196619",
  fifthRowBackgroundColor: "#005ce6",
  sixthRowBackgroundColor: "#990099",
  seventhRowBackgroundColor: "#cc0052",
  eighthRowBackgroundColor: "#2eb82e",
  ninthRowBackgroundColor: "#00cccc",
  tenthRowBackgroundColor: "#ff8080",

  rowPaddingBottom: 4,
  rowPaddingTop: 4,

  get boxHeight() {
    const totalPaddings = (this.rowPaddingBottom + this.rowPaddingTop) * 9;
    return (this.colorBoxHeight - totalPaddings) / 10.0;
  },

  columnPaddingLeft: 5,
  columnPaddingRight: 5,

  get boxWidth() {
    const totalPaddings = (this.columnPaddingLeft + this.columnPaddingRight + 3) * 11;
    return (this.colorBoxWidth - totalPaddings) / 10.0;
  },

  buttonGroupHeight: 70,
};
