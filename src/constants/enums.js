// eslint-disable-next-line import/prefer-default-export
export const BALL_STATUS = {
  NOT_THROWN: "NOT_THROWN",
  THROWING: "THROWING",
  WAITING_RESPONSE: "WAITING_RESPONSE",
  COMPLETED: "COMPLETED",
};
