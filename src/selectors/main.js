import _ from "lodash";
import {createSelector} from "reselect";
import {BALL_STATUS} from "../constants/enums";

export const getCurrentDecimal = state => state.main.currentDecimal;

export const getCounts = state => state.main.counts;

export const getCurrentDecimalBoxes = createSelector(
  [getCurrentDecimal, getCounts],
  (currentDecimal, counts) => {
    console.log("GETCURRENTDECIMALBOXES is preparing ", currentDecimal, counts);
    if (!counts) {
      return [];
    }
    const result = _.map(counts[currentDecimal].allIds, id => counts[currentDecimal].byIds[id]);
    console.log("currentDECIMAL BOXES ", result);
    return result;
  }
);

export const getThrownBoxes = state => _.filter(getCurrentDecimalBoxes(state), x => x.status !== BALL_STATUS.NOT_THROWN);

export const getActiveBox = createSelector(
  [getCurrentDecimalBoxes],
  boxes => _.find(boxes, x => x.status !== BALL_STATUS.NOT_THROWN && x.status !== BALL_STATUS.COMPLETED)
);

export const getButtonDisabled = createSelector(
  [getCurrentDecimalBoxes, getActiveBox],
  (boxes, activeBox) => {
    const anyBoxIsThrown =
      _(boxes)
        .filter(x => x.status !== BALL_STATUS.NOT_THROWN)
        .size() > 0;
    console.log("anyBoxIsThrown ", anyBoxIsThrown);
    if (!anyBoxIsThrown) return false;

    console.log("activeBox:: ", activeBox);
    console.log("!!activeBox ", !!activeBox);
    return !!activeBox;
  }
);

export const getChoicesOfBoxWaitingResponse = createSelector(
  [getCurrentDecimalBoxes, getCurrentDecimal],
  (boxes, currentDecimal) => {
    const boxWaitingResponse = _.find(boxes, x => x.status === BALL_STATUS.WAITING_RESPONSE);

    console.log("boxWaitingResponse::: ", boxWaitingResponse);
    if (!boxWaitingResponse) return undefined;

    const answer = boxWaitingResponse.id * currentDecimal;
    const choices = [answer - currentDecimal, answer, answer + currentDecimal];
    return choices;
  }
);
