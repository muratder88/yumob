import React from "react";
import {View} from "react-native";

import ColumnBackground from "../../components/ColumnBackground";
import RowBackground from "../../components/RowBackground";

import BoxContainer from "./BoxContainer";
import ButtonGroupCtr from "./ButtonGroupCtr";

const Body = () => (
  <View
    style={{
      flex: 1,
    }}
  >
    <BoxContainer />
    <ColumnBackground />
    <RowBackground />
    <ButtonGroupCtr />
  </View>
);

export default Body;
