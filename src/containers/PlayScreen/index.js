import React, {Component} from "react";
import {View, ActivityIndicator, StyleSheet} from "react-native";
import {connect} from "react-redux";

// Defaults to regular
import Body from "./Body";

import theme from "../../constants";
import {setInitialCounts, throwNewBox} from "../../actions/main";
import MultiplicationCtr from "./MultiplicationCtr";
import ChoiceCtr from "./ChoiceCtr";

// eslint-disable-next-line react/prefer-stateless-function
class PlayScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    const {setInitialCounts, throwNewBox} = this.props;
    setInitialCounts();
    setTimeout(() => {
      throwNewBox();
    }, 500);
  }

  render() {
    const {loading} = this.props;
    return (
      <View style={{flex: 1}}>
        {loading && (
          <View style={[s.main]}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        )}
        <View
          style={{
            height: theme.playScreenHeaderHeight,
            backgroundColor: "#ED553B",
          }}
        >
          <MultiplicationCtr />
          <ChoiceCtr />
        </View>
        <Body />
      </View>
    );
  }
}

export default connect(
  state => ({
    loading: typeof state.main.counts === "undefined",
  }),
  (dispatch, props) => ({
    setInitialCounts: () => dispatch(setInitialCounts()),
    throwNewBox: () => dispatch(throwNewBox(props)),
  })
)(PlayScreen);

const s = StyleSheet.create({
  main: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(52, 52, 52, 0.7)",
    elevation: 32,
    shadowColor: "#000",
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 2,
  }, // main

  // header
  multiplyItemContainer: {
    justifyContent: "center",
    alignItems: "center",
  },

  multiplyItemText: {
    fontSize: 32,
  },

  headerButtonWrapper: {
    flex: 1,
    padding: 5,
  },
});
