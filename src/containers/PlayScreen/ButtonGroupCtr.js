import React from "react";
import {connect} from "react-redux";
import _ from "lodash";

import ButtonGroup from "../../components/ButtonGroup";
import {getButtonDisabled} from "../../selectors/main";

const ButtonGroupCtr = ({disabled, throwNewBox}) => <ButtonGroup disabled={disabled} onPress={throwNewBox} />;

export default connect(state => ({
  disabled: getButtonDisabled(state),
}))(ButtonGroupCtr);
