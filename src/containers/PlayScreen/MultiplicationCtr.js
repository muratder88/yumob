import React from "react";
import {View, Text, StyleSheet} from "react-native";
import {connect} from "react-redux";
import _ from "lodash";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {getActiveBox, getCurrentDecimal} from "../../selectors/main";

const MultiplicationCtr = ({activeBox, secondDigit}) => {
  console.log("MultiplicationCtr ", activeBox);
  return (
    <View style={{flex: 4, flexDirection: "row", justifyContent: "center", alignItems: "center"}}>
      <View style={s.multiplyItemContainer}>
        <Digit number={activeBox ? activeBox.id : undefined} />
      </View>
      <View style={s.multiplyItemContainer}>
        <Icon name="close" size={80} color="white" />
      </View>
      <View style={s.multiplyItemContainer}>
        <Digit number={secondDigit} />
      </View>
      <View style={s.multiplyItemContainer}>
        <Icon name="equal" size={80} color="white" />
      </View>
      <View style={s.multiplyItemContainer}>
        <Icon name="help-circle" size={80} color="white" />
      </View>
    </View>
  );
};

const Badge = ({size, number}) => (
  <View style={{width: size, height: size, padding: 5}}>
    <View style={{borderRadius: size / 2, backgroundColor: "white", flex: 1, justifyContent: "center", alignItems: "center"}}>
      <Text style={{fontSize: ((size - 20) * 3) / 4, fontWeight: "bold", color: "#ED553B"}}>{number}</Text>
    </View>
  </View>
);

const Digit = ({number}) => {
  if (!number) return <Icon name="help-circle" size={80} color="white" />;
  return <Badge size={80} number={number} />;
};

export default connect(state => ({
  activeBox: getActiveBox(state),
  secondDigit: getCurrentDecimal(state),
}))(MultiplicationCtr);

const s = StyleSheet.create({
  // header
  multiplyItemContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
});
