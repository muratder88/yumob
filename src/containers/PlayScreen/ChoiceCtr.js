import React from "react";
import {View, StyleSheet, Button} from "react-native";
import {connect} from "react-redux";
import _ from "lodash";

import {getChoicesOfBoxWaitingResponse} from "../../selectors/main";
import {answerActiveBoxQuestion} from "../../actions/main";

const ChoiceCtr = ({choices, answerActiveBoxQuestion}) => {
  console.log("MURAT choices ", choices);
  if (!choices) return false;

  return (
    <View style={{flex: 1.5, flexDirection: "row"}}>
      {_.map(choices, (choice, index) => (
        <Choice key={index} number={choice} onPress={answerActiveBoxQuestion} />
      ))}
    </View>
  );
};

const Choice = ({number, onPress}) => (
  <View style={s.headerButtonWrapper}>
    <Button title={number.toString()} onPress={() => onPress(number)} />
  </View>
);

export default connect(
  state => ({
    choices: getChoicesOfBoxWaitingResponse(state),
  }),
  (dispatch, props) => ({
    answerActiveBoxQuestion: number => dispatch(answerActiveBoxQuestion(number, props)),
  })
)(ChoiceCtr);

const s = StyleSheet.create({
  headerButtonWrapper: {
    flex: 1,
    padding: 5,
  },
});
