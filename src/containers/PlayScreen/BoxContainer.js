import React from "react";
import {connect} from "react-redux";
import _ from "lodash";

import {changeBoxStatus} from "../../actions/main";
import {getThrownBoxes, getCurrentDecimal} from "../../selectors/main";
import Box from "../../components/Box";
import {BALL_STATUS} from "../../constants/enums";

const BoxContainer = ({boxes, decimal, onAnimationCompleted}) => {
  if (_.size(boxes) === 0) return false;
  return _.map(boxes, (props, key) => <Box key={key} {...props} decimal={decimal} onAnimationCompleted={onAnimationCompleted} />);
};

export default connect(
  state => ({
    boxes: getThrownBoxes(state),
    decimal: getCurrentDecimal(state),
  }),
  dispatch => ({
    onAnimationCompleted: id => dispatch(changeBoxStatus({id, status: BALL_STATUS.WAITING_RESPONSE})),
  })
)(BoxContainer);
