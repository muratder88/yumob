import React from "react";
import {View, Text, Button} from "react-native";

const Dashboard = props => {
  return (
    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
      <Text style={{fontSize: 23, fontWeight: "bold"}}>This is a Dashboard page</Text>
      <Button onPress={() => props.navigation.navigate("PlayScreen")} title="Play" />
    </View>
  );
};

export default Dashboard;
