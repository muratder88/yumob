/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from "react";
import {StyleSheet, Text, View, Dimensions} from "react-native";

import theme from "../constants";

const {width} = Dimensions.get("window");

const ColumnBackground = () => (
  <View style={{width, height: theme.colorBoxHeight, position: "absolute", bottom: theme.buttonGroupHeight, flexDirection: "row"}}>
    <View style={styles.mainWrapper}>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
      <View style={[styles.column]}>
        <View style={styles.columnWrapper}>
          <Text style={styles.columnText} />
        </View>
      </View>
    </View>
  </View>
);
export default ColumnBackground;
const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    flexDirection: "row",
  },
  column: {
    flex: 1,
    padding: 0,
    paddingLeft: 5,
    paddingRight: 5,
  },
  columnWrapper: {
    flex: 1,
    borderWidth: 1,
    opacity: 0.6,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 7,
  },
  columnText: {
    fontSize: 32,
    fontWeight: "bold",
  },
});
