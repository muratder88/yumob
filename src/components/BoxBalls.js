import React from "react";
import {View} from "react-native";
import _ from "lodash";

import Icon from "react-native-vector-icons/MaterialCommunityIcons"; // Defaults to regular

import theme from "../constants";

const BoxBall = () => (
  <View style={{flex: 1, justifyContent: "center", alignItems: "center", margin: 4}}>
    <Icon name="circle-slice-8" size={(theme.boxWidth * 2) / 3} color="black" />
  </View>
);

const getList = decimal => {
  /* const list = [1];
  for (let i = 0; i <= decimal; i++) {
    // eslint-disable-next-line no-unused-expressions
    list.push[i];
  }
  return list;
  */
  if (decimal === 1) return [1];
  if (decimal === 2) return [1, 2];
  if (decimal === 3) return [1, 2, 3];
  if (decimal === 4) return [1, 2, 3, 4];
  if (decimal === 5) return [1, 2, 3, 4, 5];
  if (decimal === 6) return [1, 2, 3, 4, 5, 6];
  if (decimal === 7) return [1, 2, 3, 4, 5, 6, 7];
  if (decimal === 8) return [1, 2, 3, 4, 5, 6, 7, 8];
  if (decimal === 9) return [1, 2, 3, 4, 5, 6, 7, 8, 9];
  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
};
const BoxBalls = ({decimal}) => {
  return (
    <View style={{flex: 1, flexDirection: "column"}}>
      {_.map(getList(decimal), (x, index) => (
        <BoxBall key={index} decimal={decimal} row={index + 1} />
      ))}
    </View>
  );
};

export default BoxBalls;
