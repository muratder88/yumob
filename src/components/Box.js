import React, {Component} from "react";
import {Animated} from "react-native";

import theme from "../constants";
import BoxBalls from "./BoxBalls";

class Box extends Component {
  constructor(props) {
    super(props);
    this.moveAnimation = new Animated.ValueXY({x: this.getCordinateX(), y: 0});
  }

  componentDidMount() {
    const {onAnimationCompleted, id} = this.props;
    Animated.spring(this.moveAnimation, {
      toValue: {x: this.getCordinateX(), y: this.getStopCordinateY()},
    }).start(() => {
      if (onAnimationCompleted) {
        onAnimationCompleted(id);
      }
    });
  }

  getHeight = () => {
    const {decimal} = this.props;
    return theme.boxHeight * decimal + (theme.rowPaddingBottom + theme.rowPaddingTop) * (decimal - 1);
  };

  getCordinateX = () => {
    const {id} = this.props;
    const result = (theme.colorBoxWidth / 10.0) * (id - 1) + theme.columnPaddingLeft + 2;
    return result;
  };

  getStopCordinateY = () => {
    return theme.colorBoxHeight - this.getHeight() + 5;
  };

  render() {
    const {decimal} = this.props;
    const {backgroundColor} = this.props;
    return (
      <Animated.View
        style={[
          {
            position: "absolute",
            width: theme.boxWidth,
            height: this.getHeight(),
            opacity: 0.9,
            margin: 0,
            padding: 0,
            shadowColor: "#000",
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 12,
            backgroundColor,
          },
          this.moveAnimation.getLayout(),
        ]}
      >
        <BoxBalls decimal={decimal} />
      </Animated.View>
    );
  }
}

export default Box;
