/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from "react";
import {StyleSheet, Text, View, Dimensions} from "react-native";

import theme from "../constants";

const {width} = Dimensions.get("window");

const RowBackground = () => (
  <View style={{width, zIndex: 1, opacity: 0.8, height: theme.colorBoxHeight, position: "absolute", bottom: theme.buttonGroupHeight}}>
    <View style={[styles.row, {paddingTop: 0}]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.tenthRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>
    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.ninthRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.eighthRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.seventhRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.sixthRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.fifthRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.fourthRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.thirdRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.secondRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>

    <View style={[styles.row, {paddingBottom: 0}]}>
      <View style={[styles.rowWrapper, {backgroundColor: theme.firstRowBackgroundColor}]}>
        <Text style={styles.rowText} />
      </View>
    </View>
  </View>
);

export default RowBackground;

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row",
    paddingTop: theme.rowPaddingTop,
    paddingBottom: theme.rowPaddingBottom,
  },
  rowWrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  rowText: {
    fontSize: 32,
    fontWeight: "bold",
  },
});
