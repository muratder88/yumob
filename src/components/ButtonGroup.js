/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from "react";
import {StyleSheet, TouchableOpacity, View, Text, Dimensions} from "react-native";

import theme from "../constants";

const ButtonGroup = ({disabled, onPress}) => (
  <View style={s.buttonConatiners}>
    <TouchableOpacity
      activeOpacity={disabled ? 1 : 0.7}
      onPress={() => {
        if (!disabled) onPress();
      }}
      style={s.button}
    >
      <View style={s.buttonWrapper}>
        <Text style={s.buttonText}>Button</Text>
      </View>
    </TouchableOpacity>
  </View>
);

export default ButtonGroup;

const s = StyleSheet.create({
  buttonConatiners: {
    position: "absolute",
    bottom: 0,
    height: theme.buttonGroupHeight,
    width: Dimensions.get("window").width,
    paddingTop: 5,
  },
  button: {
    flex: 1,
    backgroundColor: "#173F5F",
  },
  buttonWrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
  },
});
