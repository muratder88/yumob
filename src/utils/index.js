// eslint-disable-next-line import/prefer-default-export
export const getDecimalIcon = decimal => {
  if (decimal < 10) return `numeric-${decimal}-circle`;
  return "numeric-10-circle";
};
