/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from "react";
import {Provider} from "react-redux";
import {createStore, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import {createStackNavigator, createAppContainer} from "react-navigation";

import rootReducer from "./reducers";
import PlayScreen from "./containers/PlayScreen";
import Dashboard from "./containers/Dashboard";

const middlewares = [thunk, logger];
const store = createStore(rootReducer, {}, applyMiddleware(...middlewares));

// eslint-disable-next-line react/prefer-stateless-function
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    PlayScreen,
    Dashboard,
  },
  {
    initialRouteName: "Dashboard",
  }
);

const AppContainer = createAppContainer(AppNavigator);
