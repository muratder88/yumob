import * as types from "../constants/mainActionTypes";
import theme from "../constants";
import {BALL_STATUS} from "../constants/enums";

const getCounts = () => {
  const counts = {};
  for (let i = 1; i <= 10; i++) {
    counts[i] = {};
    counts[i].byIds = {};
    counts[i].allIds = [];
    for (let x = 1; x <= 10; x++) {
      counts[i].byIds[x] = {id: x, backgroundColor: theme.rowBackgroundColors[i], status: BALL_STATUS.NOT_THROWN};
      counts[i].allIds.push(x);
    }
  }

  return counts;
};

const initialState = {
  currentDecimal: 1,
  activeBoxId: 0,
  counts: undefined,
};

console.log("initialState ", initialState);

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.INCREMENT_CURRENT_DECIMAL: {
      return {
        ...state,
        currentDecimal: state.currentDecimal + 1,
      };
    }

    case types.SET_INITAL_COUNTS: {
      return {
        ...state,
        counts: getCounts(),
      };
    }

    case types.DESTROY_COUNTS: {
      return {
        ...state,
        counts: undefined,
      };
    }

    case types.SET_CURRENT_DECIMAL: {
      return {
        ...state,
        currentDecimal: action.payload.currentDecimal,
      };
    }

    case types.CHANGE_BOX_STATUS: {
      const {id, status} = action.payload;
      const {currentDecimal} = state;
      return {
        ...state,
        activeBoxId: id,
        counts: {
          ...state.counts,
          [currentDecimal]: {
            ...state.counts[currentDecimal],
            byIds: {
              ...state.counts[currentDecimal].byIds,
              [id]: {
                ...state.counts[currentDecimal].byIds[id],
                status,
              },
            },
          },
        },
      };
    }

    default:
      return state;
  }
};

export default reducer;
