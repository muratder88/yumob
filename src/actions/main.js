import _ from "lodash";
import {Alert} from "react-native";

import * as types from "../constants/mainActionTypes";
import {getCurrentDecimalBoxes, getCurrentDecimal, getActiveBox} from "../selectors/main";
import {BALL_STATUS} from "../constants/enums";

export const setInitialCounts = () => ({
  type: types.SET_INITAL_COUNTS,
});

export const destroyCounts = () => ({
  type: types.DESTROY_COUNTS,
});

export const incrementCurrentDecimal = () => ({
  type: types.INCREMENT_CURRENT_DECIMAL,
});

export const setCurrentDecimal = currentDecimal => ({
  type: types.SET_CURRENT_DECIMAL,
  payload: {currentDecimal},
});

export const changeBoxStatus = ({status, id}) => ({
  type: types.CHANGE_BOX_STATUS,
  payload: {status, id},
});

export const answerActiveBoxQuestion = (answer, props) => (dispatch, getState) => {
  const state = getState();
  const activeBox = getActiveBox(state);
  const currentDecimal = getCurrentDecimal(state);

  console.log("answer::", answer);
  console.log("typeof answer:::", typeof answer);
  console.log("activeBox.id:::", activeBox.id);
  console.log("currentDecimal:::", currentDecimal);
  if (answer === activeBox.id * currentDecimal) {
    dispatch(
      changeBoxStatus({
        id: activeBox.id,
        status: BALL_STATUS.COMPLETED,
      })
    );

    if (activeBox && activeBox.id !== 10) {
      // only throw next mutliplier
      dispatch(throwNewBox());
      return;
    }

    if (currentDecimal !== 10) {
      // change multiplicand
      dispatch(incrementCurrentDecimal());
      setTimeout(() => dispatch(throwNewBox()), 500);
      return;
    }

    // multiplicand=10 and multiplier=10 so game is over
    Alert.alert("Oyun Bitti", "Tekrardan oymak isterminiz?", [
      {
        text: "EVET",
        onPress: () => dispatch(restartGame()),
      },
      {
        text: "HAYIR",
        onPress: () => {
          dispatch(destroyCounts());
          dispatch(setCurrentDecimal(1));
          props.navigation.navigate("Dashboard");
        },
        style: "cancel",
      },
    ]);
  } else {
    alert("Yanlış cevap");
  }
};

export const throwNewBox = () => (dispatch, getState) => {
  const state = getState();

  const currentBoxes = getCurrentDecimalBoxes(state);

  const notThrowedBoxes = _.filter(currentBoxes, x => x.status === BALL_STATUS.NOT_THROWN);

  const activeBox = notThrowedBoxes[0];

  dispatch(changeBoxStatus({status: BALL_STATUS.THROWING, id: activeBox.id}));
};

export const restartGame = () => dispatch => {
  dispatch(destroyCounts());
  dispatch(setCurrentDecimal(1));
  setTimeout(() => {
    dispatch(setInitialCounts());
  }, 1000);
};
